package ku.util;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * 
 * @author Voraton Lertrattanapaisal
 *
 *For Create Array Iterator.
 *
 * @param <T> is Data type.
 */
public class ArrayIterator<T> implements Iterator<T> {
	/** array for keep object.*/
	private T[ ] array;
	/** cursor for tell which index are here.*/
	private int cursor;
	/**
	 * Initialize new ArrayIterator.
	 * @param array for set array value to use array iterator.
	 */
	public ArrayIterator (T[] array){
		this.array = array;
		cursor=0;
	}
	/**
	 * For check is null in next index.
	 */
	public boolean hasNext(){
		
		int cursor = this.cursor;
		while (cursor<array.length){
			if (array[cursor]!=null){return true;}
			else {cursor++;}
		}
		return false;
		
	}
	/**
	 * For return object of next index.
	 */
	public T next(){
		while(hasNext()){
			if(array[cursor]==null)
			{
				cursor++;
			}
			else { T obj = array[cursor];
			cursor++; 
			return obj;  }
		}
		throw new NoSuchElementException( );
	}
	/**
	 * For Remove last object in last index
	 */
	public void remove(){
		array[cursor-1]=null;
	}
}
