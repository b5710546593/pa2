package ku.util;

import java.util.EmptyStackException;
/**
 * 
 * @author Voraton Lertrattanapaisal
 *
 *For create stack to keep data.
 *
 * @param <T> Data type.
 */
public class Stack<T> {
	/** array for keep object.*/
	private T[] items; 
	/**
	 * Initialize stack for keep object.
	 * @param capacity capacity of stack.
	 */
	public Stack(int capacity) {
		if (capacity>0){
			items = (T[]) new Object[capacity];
		}
		else {
			items = (T[]) new Object[]{};
		}
	}
	/**
	 * To return capacity of stack.
	 * @return capacity of stack.
	 */
	public int capacity(){
		if (items.length>=0){
			return items.length;
		}
		else {
			return -1;
		}
	}
	/**
	 * To check is stack empty.
	 * @return true if stack is empty else return false.
	 */
	public boolean isEmpty(){
		for (int i = 0;i<items.length;i++){
			if(items[i]!=null){
				return false;
			}
		}
		return true;
	}
	/**
	 * To check is stack full.
	 * @return true if stack is full else return false.
	 */
	public boolean isFull(){
		for (int i = 0;i<items.length;i++){
			if(items[i]==null){
				return false;
			}
		}
		return true;
	}
	/**
	 * To get the object on the top of stack without remove it from stack.
	 * @return object which is on the top.
	 */
	public T peek(){
		if (isEmpty()){
			return null;
		}
		else {
			int index = 0;
			for (int i = items.length-1;i>=0;i--){
				if(items[i]!=null){
					index=i;
					break;
				}
			}
			return 	items[index];
		}
	}
	/**
	 * To get the object on the top of stack and remove it from stack.
	 * @return object on the top of stack.
	 */
	public T pop(){
		if(isEmpty()){
			throw new EmptyStackException();
		}
		else {
			int index = 0;
			for (int i = items.length-1;i>=0;i--){
				if(items[i]!=null){
					index = i;
					break;
				}
			}
			T obj = items[index];
			items[index]=null;
			return 	obj;
		}
	}
	/**
	 * To add object to the top of stack.
	 * @param obj object that want to be add.
	 */
	public void push( T obj ) {
		if (obj == null ){
			throw new IllegalArgumentException();
		}
		if (!isFull()){
			for (int i = 0 ;i<items.length;i++){
				if (items[i]==null){
					items[i]=obj;
					System.out.println(items[i]);
					break;
				}
			}
		}
	}
	/**
	 * For return number of object in stack.
	 * @return number of object in stack.
	 */
	public int size( ) {
		if (isEmpty()){
			return 0;
		}
		else {
			int count = 0;
			for (int i =0;i<items.length;i++){
				if(items[i]!=null){
					count++;
				}
			}
			return count;
		}
	}
}
